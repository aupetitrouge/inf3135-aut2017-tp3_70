SRC_DIR = src
BIN_DIR = bin
EXEC = tp3

.PHONY: exec clean source

exec: source bindir
	mv $(SRC_DIR)/tp3 $(BIN_DIR)

bindir:
	mkdir -p $(BIN_DIR)

clean:
	$(MAKE) clean -C $(SRC_DIR)
	rm -rf $(BIN_DIR)

source:
	$(MAKE) -C $(SRC_DIR)
